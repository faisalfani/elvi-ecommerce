import { Button, Flex, Heading, Image, Link } from "@chakra-ui/react";
import React from "react";

const CollectionSection = ({ data }) => {
  return (
    <>
      <Flex>
        <Link
          padding="3rem"
          direction="column"
          border="1px"
          borderColor="gray.200"
          className="collection__left-section"
        >
          <Image
            src={data.collection_image_left}
            height="90%"
            alt="Segun Adebayo"
            width="auto"
            objectFit="cover"
          />
          <Heading
            color="black"
            fontWeight="extrabold"
            fontSize="2rem"
            mx="2rem"
            mt="1rem"
            textDecoration="none"
          >
            {"MEN'S FALL-WINTER 2021 FASHION SHOW IN SEOUL"}
          </Heading>
        </Link>
        <Flex direction="column" width="50%">
          <Link
            className="collection__right-section"
            padding="3rem"
            border="1px"
            height="50%"
            borderColor="gray.200"
          >
            <Heading fontSize="2rem" fontWeight="extrabold">
              Book An Appointment
            </Heading>
            <Button
              borderRadius="none"
              className="button--primary"
              color="white"
              backgroundColor="black"
              width="40%"
            >
              Find your nearest store
            </Button>
          </Link>
          <Link
            className="collection__right-section"
            padding="3rem"
            border="1px"
            borderColor="gray.200"
          >
            <Image
              src={data.collection_image_right}
              alt="Segun Adebayo"
              width="auto"
              objectFit="cover"
            />
            <Heading
              color="black"
              fontWeight="extrabold"
              fontSize="2rem"
              mx="2rem"
              mt="1rem"
              textDecoration="none"
            >
              {"MEN'S FALL-WINTER 2021 FASHION SHOW IN SEOUL"}
            </Heading>
          </Link>
        </Flex>
      </Flex>
      <Flex className="mini-collection">
        <Link
          className="mini-collection__item"
          padding="2rem 3rem"
          border="1px"
          borderColor="gray.200"
        >
          <Image
            src={data.collection_item1}
            alt="Segun Adebayo"
            width="auto"
            objectFit="cover"
          />
          <Heading
            color="black"
            fontWeight="extrabold"
            fontSize="1.2rem"
            mx="1rem"
            mt="1rem"
            textDecoration="none"
          >
            LV SQUAD AND LV SUNSET, THE NEW ICONIC SHOE DESIGNS Louis Vuitton
            presents the LV Squad
          </Heading>
        </Link>
        <Link
          className="mini-collection__item"
          padding="2rem 3rem"
          border="1px"
          borderColor="gray.200"
        >
          <Image
            src={data.collection_item2}
            alt="Segun Adebayo"
            width="auto"
            objectFit="cover"
          />
          <Heading
            color="black"
            fontWeight="extrabold"
            fontSize="1.2rem"
            mx="1rem"
            mt="1rem"
            textDecoration="none"
          >
            New Sunglasses
          </Heading>
        </Link>
        <Link
          className="mini-collection__item"
          padding="2rem 3rem"
          border="1px"
          borderColor="gray.200"
        >
          <Image
            src={data.collection_item3}
            alt="Segun Adebayo"
            width="auto"
            objectFit="cover"
          />
          <Heading
            color="black"
            fontWeight="extrabold"
            fontSize="1.2rem"
            mx="1rem"
            mt="1rem"
            textDecoration="none"
          >
            Cruise 22 show
          </Heading>
        </Link>
      </Flex>
    </>
  );
};

export default CollectionSection;
