import { Box, Button, Flex, Heading, Image, Skeleton } from "@chakra-ui/react";
import React from "react";

const HeroSection = ({ data }) => {
  return (
    <Flex as="section" direction="column" height="100vh">
      <Box>
        <Skeleton
          isLoaded={data.hero_image !== undefined ? true : false}
          height={data.hero_image !== undefined ? "auto" : "50vh"}
        >
          <Image
            src={data.hero_image}
            alt="Segun Adebayo"
            width="100vw"
            height="auto"
          />
        </Skeleton>
      </Box>
      <Flex
        height="30vh"
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Heading fontWeight="extrabold" fontSize="2rem" color="black" mb="1rem">
          MIRANDA KERR AND THE CAPUCINES
        </Heading>
        <Flex>
          <Button
            borderRadius="none"
            backgroundColor="black"
            color="white"
            className="button--primary"
            mr="1rem"
          >
            Discover the Collection
          </Button>
          <Button
            borderRadius="none"
            border="1px"
            borderColor="black"
            backgroundColor="white"
          >
            Discover the Collection
          </Button>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default HeroSection;
