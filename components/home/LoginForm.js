import React, { useState } from "react";

import {
  Drawer,
  DrawerContent,
  DrawerOverlay,
  DrawerHeader,
  Flex,
  Heading,
  Link,
  DrawerBody,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Button,
  Divider,
  Text,
} from "@chakra-ui/react";
import {
  AiOutlineClose,
  AiOutlineEye,
  AiOutlineEyeInvisible,
} from "react-icons/ai";

const LoginForm = ({ formik, isOpen, onClose, onOpen }) => {
  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  return (
    <Drawer
      onClose={() => {
        onClose();
        formik.resetForm();
      }}
      isOpen={isOpen}
      size="md"
    >
      <DrawerOverlay />
      <DrawerContent>
        <DrawerHeader>
          <Flex justifyContent="space-between" alignItems="center">
            <Heading size="md" fontWeight="extrabold" color="black">
              IDENTIFICATION
            </Heading>
            <Link
              className="nav__profile"
              color="black"
              onClick={() => onClose()}
              fontWeight="bold"
            >
              <AiOutlineClose size="1.5rem" />
            </Link>
          </Flex>
        </DrawerHeader>
        <DrawerBody>
          <Flex justifyContent="space-between" direction="column" height="100%">
            <Flex direction="column">
              <Heading
                color="black"
                size="sm"
                fontWeight="extrabold"
                mt="3rem"
                mb="1.5rem"
              >
                I ALREADY HAVE AN ACCOUNT
              </Heading>
              <FormControl
                isInvalid={formik.errors.email && formik.touched.email}
                id="email"
                color="black"
                isRequired
                mb="1.5rem"
              >
                <FormLabel fontWeight="light" letterSpacing="tight">
                  Email
                </FormLabel>
                <Input
                  onKeyPress={(e) => {
                    if (e.key == "Enter") {
                      formik.submitForm();
                    }
                  }}
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  type="email"
                  placeholder="Enter Email"
                />
              </FormControl>
              <FormControl
                isInvalid={formik.errors.email && formik.touched.email}
                id="password"
                color="black"
                isRequired
                mb="1.5rem"
              >
                <FormLabel fontWeight="light" letterSpacing="tight">
                  Password
                </FormLabel>
                <InputGroup size="md">
                  <Input
                    onKeyPress={(e) => {
                      if (e.key == "Enter") {
                        formik.submitForm();
                      }
                    }}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                    pr="4.5rem"
                    type={show ? "text" : "password"}
                    placeholder="Enter password"
                  />
                  <InputRightElement width="4.5rem">
                    <button onClick={handleClick}>
                      {show ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
                    </button>
                  </InputRightElement>
                </InputGroup>
                <Link fontSize="xs" color="gray.500">
                  Forgot your password?
                </Link>
              </FormControl>
              <Button
                borderRadius="none"
                className="button--primary"
                backgroundColor="black"
                color="white"
                fontWeight="normal"
                onClick={() => formik.submitForm()}
                isLoading={formik.isSubmitting}
                loadingText="Submitting"
              >
                Sign In
              </Button>
            </Flex>

            <Divider />

            <Flex
              direction="column"
              height="25%"
              justifyContent="space-between"
              mb="4rem"
            >
              <Heading fontSize="md" fontWeight="extrabold">
                {"I DON'T HAVE AN ACCOUNT"}
              </Heading>
              <Text fontSize="md">
                Enjoy added benefits and a richer experience by creating a
                personal account
              </Text>
              <Button
                borderRadius="none"
                className="button--primary"
                backgroundColor="black"
                color="white"
              >
                Create ElVi Account
              </Button>
            </Flex>
          </Flex>
        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
};

export default LoginForm;
