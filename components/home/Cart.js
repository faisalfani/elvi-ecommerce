import React, { useState } from "react";

import {
  Drawer,
  DrawerContent,
  DrawerOverlay,
  DrawerHeader,
  Flex,
  Heading,
  Link,
  DrawerBody,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Button,
  Divider,
  Text,
  Box,
  Skeleton,
} from "@chakra-ui/react";
import {
  AiOutlineClose,
  AiOutlineDelete,
  AiOutlineEye,
  AiOutlineEyeInvisible,
  AiOutlineMinus,
  AiOutlinePlus,
} from "react-icons/ai";
import { IoTicketOutline } from "react-icons/io5";

const Cart = ({ formik, isOpen, onClose, onOpen }) => {
  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);
  const [voucher, setVoucher] = useState(false);
  return (
    <Drawer
      onClose={() => {
        onClose();
      }}
      isOpen={isOpen}
      size="md"
    >
      <DrawerOverlay />
      <DrawerContent>
        <DrawerHeader>
          <Flex justifyContent="space-between" alignItems="center">
            <Heading size="md" fontWeight="extrabold" color="black">
              YOUR CART
            </Heading>
            <Link
              className="nav__profile"
              color="black"
              onClick={() => onClose()}
              fontWeight="bold"
            >
              <AiOutlineClose size="1.5rem" />
            </Link>
          </Flex>
        </DrawerHeader>
        <DrawerBody>
          <Flex
            py="2rem"
            direction="column"
            justifyContent="space-between"
            height="100%"
          >
            <Box>
              {[1, 3].map((data) => {
                return (
                  <>
                    <Flex
                      mb="10px"
                      className="product"
                      justifyContent="space-between"
                    >
                      <Flex justifyContent="space-between">
                        <Flex
                          justifyContent="row"
                          justifyItems="start"
                          alignItems="start"
                          height="200px"
                        >
                          <Skeleton height="inherit" mr="5px" width="120px" />
                          <Flex
                            height="inherit"
                            direction="column"
                            justifyContent="space-between"
                            width="25%"
                          >
                            <Flex direction="column">
                              <Skeleton mb="10px">
                                <Text>lorem ipsum</Text>
                              </Skeleton>
                              <Skeleton mb="10px">
                                <Text>S</Text>
                              </Skeleton>
                              <Skeleton mb="10px">
                                <Text>Rp 10000</Text>
                              </Skeleton>
                            </Flex>
                            <Flex
                              padding="2px 6px"
                              alignItems="center"
                              border="1px"
                              borderColor="black"
                              fontSize="sm"
                              justifyContent="space-between"
                            >
                              <Box cursor="pointer">
                                <AiOutlineMinus />
                              </Box>
                              <Input
                                readOnly
                                fontSize="sm"
                                textAlign="center"
                                type="text"
                                height="2rem"
                                border="none"
                                value="1"
                              />
                              <Box cursor="pointer">
                                <AiOutlinePlus />
                              </Box>
                            </Flex>
                          </Flex>
                        </Flex>
                      </Flex>
                      <Box cursor="pointer" color="red.700">
                        <AiOutlineDelete size="1rem" />
                      </Box>
                    </Flex>
                  </>
                );
              })}
            </Box>

            <Flex
              className="checkout"
              direction="column"
              justifyContent="start"
            >
              <Flex alignItems="center" cursor="pointer" fontSize="sm">
                <IoTicketOutline style={{ marginRight: "8px" }} size="1rem" />{" "}
                {voucher ? (
                  <InputGroup>
                    <Input type="text" placeholder="Voucher Code here" />
                    <InputRightElement onClick={() => setVoucher(false)}>
                      <AiOutlineClose />
                    </InputRightElement>
                  </InputGroup>
                ) : (
                  <Text
                    onClick={() => setVoucher(true)}
                    textDecoration="underline"
                  >
                    Have a Voucher ?
                  </Text>
                )}
              </Flex>

              <Button
                mt="1rem"
                borderRadius="none"
                className="button--primary"
                backgroundColor="black"
                color="white"
                fontWeight="normal"
                loadingText="Submitting"
              >
                Check Out
              </Button>
            </Flex>
          </Flex>
        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
};

export default Cart;
