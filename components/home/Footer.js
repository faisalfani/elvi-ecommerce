import { Box, Divider, Flex, Heading, Link, Text } from "@chakra-ui/react";
import React from "react";
import { AiOutlineGlobal } from "react-icons/ai";

const Footer = () => {
  return (
    <Flex
      padding="3rem"
      direction="column"
      height="25vh"
      backgroundColor="black"
      justifyContent="space-around"
    >
      <Box mb="2rem">
        <Heading color="white" fontSize="1.5rem" fontWeight="semibold">
          ElViton
        </Heading>
      </Box>
      <Divider />
      <Flex color="white" width="80%" mt="2rem" justifyContent="space-between">
        <Link textDecoration="none" whiteSpace="nowrap">
          <Flex alignItems="baseline">
            <AiOutlineGlobal
              style={{ display: "inline-block", marginRight: "5px" }}
              size="1rem"
            />
            English (INTL)
          </Flex>
        </Link>
        <Link textDecoration="none">Contact</Link>
        <Link textDecoration="none">About Us</Link>
        <Link textDecoration="none">Follow Us</Link>
        <Link textDecoration="none">Legal & Privacy</Link>
        <Link textDecoration="none">Career</Link>
        <Text>Handle With 💗 Faisal 2020©</Text>
      </Flex>
    </Flex>
  );
};

export default Footer;
