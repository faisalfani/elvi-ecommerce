import {
  Box,
  Button,
  Collapse,
  Container,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  Link,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Skeleton,
  Slide,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import {
  AiOutlineLogout,
  AiOutlineRight,
  AiOutlineSearch,
  AiOutlineShoppingCart,
  AiOutlineUser,
} from "react-icons/ai";

import { useFormik } from "formik";
import Cart from "./Cart";

const Navbar = ({ onOpen, session, signOut }) => {
  const {
    isOpen: isSearch,
    onOpen: onSearchOpen,
    onClose: onSearchClose,
  } = useDisclosure();
  const {
    isOpen: isCartOPen,
    onOpen: onCartOpen,
    onClose: onCartClose,
  } = useDisclosure();

  const formik = useFormik({
    initialValues: { search: "" },
    onSubmit: async (values) => {
      console.log(values);
    },
  });
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box boxShadow="md">
      <Container maxW="container.xl">
        <Flex
          width="100%"
          height="16"
          alignItems="center"
          justifyContent="space-between"
          as="nav"
        >
          <Heading size="lg" color="black" letterSpacing="tight">
            Elvi
          </Heading>
          <Flex alignItems="center" className="nav__item-wrapper">
            <Link className="nav__search" onClick={() => onSearchOpen()}>
              <AiOutlineSearch
                size="20px"
                style={{ display: "inline-block", marginRigh: "2px" }}
              />{" "}
              Search
            </Link>
            <Link
              position="relative"
              className="nav__search"
              onClick={() => onCartOpen()}
            >
              <AiOutlineShoppingCart size="20px" />
              {session ? (
                <Box
                  position="absolute"
                  width="18px"
                  height="18px"
                  borderRadius="100px"
                  top="-10px"
                  right="-10px"
                  zIndex="100"
                  backgroundColor="black"
                  color="white"
                  border="2px"
                  borderColor="white"
                >
                  <Flex
                    width="100%"
                    height="100%"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Text fontSize="xx-small" fontWeight="bold">
                      1
                    </Text>
                  </Flex>
                </Box>
              ) : null}
            </Link>
            {!session ? (
              <Link className="nav__profile" onClick={() => onOpen()}>
                <AiOutlineUser size="20px" />
              </Link>
            ) : (
              <Menu>
                <MenuButton rightIcon={<AiOutlineRight />}>
                  Hello, {session.user.email}
                </MenuButton>
                <MenuList>
                  <MenuItem onClick={signOut}>
                    <Flex alignItems="center">
                      <AiOutlineLogout style={{ marginRight: "10px" }} />
                      Sign out
                    </Flex>
                  </MenuItem>
                </MenuList>
              </Menu>
            )}
          </Flex>
        </Flex>
      </Container>
      <Modal
        onClose={() => {
          onSearchClose();
          formik.resetForm();
        }}
        size="4xl"
        isOpen={isSearch}
        boxShadow="md"
        isCentered
      >
        <ModalOverlay />
        <ModalContent py="1rem" position="relative">
          <ModalHeader>
            <InputGroup>
              <Input
                type="text"
                placeholder="Search"
                onChange={formik.handleChange}
                name="search"
                value={formik.values.search}
              />
              <InputLeftElement>
                <AiOutlineSearch />
              </InputLeftElement>
            </InputGroup>
          </ModalHeader>
          <Collapse
            in={formik.values.search !== "" ? true : false}
            animateOpacity
          >
            <Box px="2rem" pb="2rem" color="white" mt="4" rounded="md">
              <Heading color="black" fontSize="lg" my="1rem">
                Products
              </Heading>
              <Flex className="search__search-product">
                <Flex direction="column">
                  <Skeleton height="8rem" width="8rem" mb="10px" />
                  <Skeleton>
                    <Heading fontSize="md" fontWeight="normal" color="black">
                      Lorem Ipsum
                    </Heading>
                  </Skeleton>
                </Flex>
                <Flex direction="column">
                  <Skeleton height="8rem" width="8rem" mb="10px" />
                  <Skeleton>
                    <Heading fontSize="md" fontWeight="normal" color="black">
                      Lorem Ipsum
                    </Heading>
                  </Skeleton>
                </Flex>
                <Flex direction="column">
                  <Skeleton height="8rem" width="8rem" mb="10px" />
                  <Skeleton>
                    <Heading fontSize="md" fontWeight="normal" color="black">
                      Lorem Ipsum
                    </Heading>
                  </Skeleton>
                </Flex>
                <Flex direction="column">
                  <Skeleton height="8rem" width="8rem" mb="10px" />
                  <Skeleton>
                    <Heading fontSize="md" fontWeight="normal" color="black">
                      Lorem Ipsum
                    </Heading>
                  </Skeleton>
                </Flex>
                <Flex direction="column">
                  <Skeleton height="8rem" width="8rem" mb="10px" />
                  <Skeleton>
                    <Heading fontSize="md" fontWeight="normal" color="black">
                      Lorem Ipsum
                    </Heading>
                  </Skeleton>
                </Flex>
              </Flex>
            </Box>
          </Collapse>
        </ModalContent>
      </Modal>
      <Cart isOpen={isCartOPen} onClose={onCartClose} onOpen={onCartOpen} />
    </Box>
  );
};

export default Navbar;
