import { useState, useEffect } from "react";
import { supabase } from "../utils/supabaseClient";
import Head from "next/head";
import {
  Button,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { useFormik } from "formik";
import * as yup from "yup";
import HeroSection from "../components/home/HeroSection";
import CollectionSection from "../components/home/CollectionSection";
import Footer from "../components/home/Footer";
import Navbar from "../components/home/Navbar";
import LoginForm from "../components/home/LoginForm";

export default function Home() {
  const [data, setData] = useState({});
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [session, setSession] = useState(null);
  const toast = useToast();
  useEffect(() => {
    loadHandler();
  }, []);

  const loadHandler = async () => {
    setSession(supabase.auth.session());

    await supabase.auth.onAuthStateChange((_event, session) => {
      setSession(session);
    });
    const { data, error } = await supabase.from("assets").select();
    data.map((datas) => {
      setData((prev) => {
        return { ...prev, [datas.name]: datas.asset };
      });
    });
  };

  const signOut = async () => {
    const { error } = await supabase.auth.signOut();
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: yup.object({
      email: yup.string().email().required("Required"),
      password: yup.string().required("Required"),
    }),
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      await submitHandler(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
  });

  const submitHandler = async (values) => {
    try {
      const { user, session, error } = await supabase.auth.signIn({
        email: values.email,
        password: values.password,
      });
      onClose();
      formik.resetForm();
      toast({
        position: "top",
        title: "Login Success.",
        description: `Welcome ${user.email}`,
        status: "success",
        duration: 2000,
        isClosable: true,
      });
      console.log(error);
    } catch (error) {
      toast({
        position: "top",
        title: "Error.",
        description: `${error.message}`,
        status: "error",
        duration: 2000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <Head>
        <title>Elvi</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Navbar session={session} signOut={signOut} onOpen={onOpen} />
      <HeroSection data={data} />
      <CollectionSection data={data} />
      <Footer />
      <LoginForm
        formik={formik}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
      />
    </>
  );
}
